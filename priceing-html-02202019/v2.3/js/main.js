$(document).ready(function(){
    jk_sticky_tab_bar(); // make the tab bar sticky.
    
    jk_fix_table_row_txt(); // copy text from main table to fixed table
    jk_scroll_fix_tables(); // synchronous horizontal scroll price tables

    jk_hide_select_buttons(); // Show/hide select buttons based on user current packages
    
    jk_change_plan_length(); // month, quarterly, annually
   // jk_change_plan_length2(); // 7day, 15day, 30day
    $('select').selectpicker();

    $('.jk_tooltip').tooltipster({
        maxWidth : 400,
        interactive: true,
        side: 'right',
        trigger: 'custom',
        triggerOpen: {
            //click: true,
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            tap: true,
            mouseleave: true,
            scroll: true,
            //touchleave: true
        }
    }); // adv tooltip
    $('.jk_tooltip-top').tooltipster({
        maxWidth : 400,
        interactive: true,
        side: 'top',
        trigger: 'custom',
        triggerOpen: {
            //click: true,
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            tap: true,
            mouseleave: true,
            scroll: true,
            //touchleave: true
        }
    }); // adv tooltip
}) // doc.ready


function jk_fix_table_row_txt(){
    var x = $('#table_one tr.tr_price').clone();
    var y = x.find('td').first().html();
    x.find('td').first().remove();
    // console.log(y);

    
    $('#table_zero tr').html('<td></td>' + x.html());
    $('#table_before_zero').html(y);
   
    // $('select').selectpicker('refresh');
} // jk_fix_table_row_txt..

function jk_scroll_fix_tables(){
    var $divs = $('#table_zero, #table_one');
    var sync = function (e) {
        var me = $(this);
        var $other = $divs.not(me).off('scroll');
        $divs.not(me).each(function (index) {
            $(this).scrollLeft(me.scrollLeft());
        });
        setTimeout(function () {
            $other.on('scroll', sync);
        }, 10);
    }
    $divs.on('scroll', sync);
} // jk_scroll_fix_tables


function jk_change_plan_length(){ // month, quarterly, annually
    var bothselects = $("#table_before_zero select.plan_type, #table_one select.plan_type");
    $(document).on('change', '#table_before_zero select.plan_type, #table_one select.plan_type', function() {
        //bothselects.val(this.value);
        bothselects.not(this).val( $(this).val() );
        console.log($(this).val());
        $('select.plan_type').selectpicker('refresh');

        // do whatever else you need to do
    });


    $('select.plan_type').prop('selectedIndex',0);
    x($('select.plan_type'));
    $(document).on('change', 'select.plan_type', function(event){
        var $this = $(this);
        x($this);
    });
    function x($this){

        var plan_length = $this.val();
        var plan_type = $this.data('ptype');

        var selected = $this.find('option:selected');
        var lite_url = selected.data('lite'); 
        var premium_url = selected.data('premium'); 
        var premium_plus_url = selected.data('pplus'); 
        // console.log(premium_url);

        $this.closest('.price_table').find('.ptype.' + plan_type + '>.plength').addClass('hide');
        $this.closest('.price_table').find('.ptype.' + plan_type + '>.' + plan_length).removeClass('hide');
        
        $('.subs_lite').attr("href", lite_url); // change button url
        $('.subs_premium').attr("href", premium_url); // change button url
        $('.subs_premium_plus').attr("href", premium_plus_url); // change button url
    }
   
} // jk_change_plan_length   ... month, quarterly, annually



/* 
function jk_change_plan_length2(){ // 7day, 15day, 30day
    $('select.payasyougo').prop('selectedIndex',0);
    x($('select.payasyougo'));
    $(document).on('change', 'select.payasyougo', function(event){
        var $this = $(this);
        x($this);
    });
    function x($this){
        var plan_type = $this.closest('.price_table').find('select.pa_type').val();
        var plan_length = $this.closest('.price_table').find('select.pa_length').val();

        var selected = $this.closest('.price_table').find('select.pa_length option:selected');
        var lite_url = selected.data('lite'); 
        var premium_url = selected.data('premium'); 
        var premium_plus_url = selected.data('pplus'); ;

        $this.closest('.price_table').find('.ptype>.plength').addClass('hide');
        $this.closest('.price_table').find('.ptype.' + plan_type + '>.' + plan_length).removeClass('hide');

        if('ptype_2' == plan_type){
            $('#payas_sel_button').attr("href", premium_url);
        }else if('ptype_3' == plan_type){
            $('#payas_sel_button').attr("href", premium_plus_url);
        }else{
            $('#payas_sel_button').attr("href", lite_url);

        }
    }
} // jk_change_plan_length   ... 7day, 15day, 30day */


function jk_sticky_tab_bar(){ // make the tab bar sticky.
    var table =$('#table_one');
    var table_0 =$('#table_zero, #table_before_zero');
    //var table_before_zero =$('#table_before_zero');
    
    var table_loc = table.offset().top;
    var table_height = table.height();
    var table_0_height = table_0.height();
    var sticky_to = table_loc + table_height - 250;
    
    
    $(window).scroll(function(){
        if ($(".mobile").is(":visible") == true) {
            var $this = $(this);
            var x = $this.scrollTop();
            if(x>=table_loc && x< sticky_to){
                
                table_0.addClass('stuck');
                table_0.removeClass('fade');
            }else{
                if(x>=sticky_to){
                    table_0.addClass('fade');
                }else{
                    table_0.removeClass('stuck');
                }
            }
        }
    });
} // jk_sticky_tab_bar, make the tab bar sticky.


function jk_hide_select_buttons(){ // Show/hide select buttons based on user current packages
    if($('.user_package[data-package]').length){
        var user_package = $('.user_package[data-package]').data('package');
        if('subs_lite' == user_package){
            $('.btn.subs_premium_plus').show().css("display", "inline-block");
            $('.btn.subs_premium').show().css("display", "inline-block");
        }else if('subs_premium' == user_package){
            $('.btn.subs_premium_plus').show().css("display", "inline-block");
        }else if('subs_premium_plus' == user_package){
            
        }else{
            $(".btn[class*='subs_']").show().css("display", "inline-block");
        }
    }else{
        $(".btn[class*='subs_']").show().css("display", "inline-block");
    }
} //jk_hide_select_buttons  ...Show/hide select buttons based on user current packages
