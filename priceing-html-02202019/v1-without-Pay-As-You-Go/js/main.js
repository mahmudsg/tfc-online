$(document).ready(function(){
    //jk_change_plan_type(); // Subs or PAYG
    jk_change_plan_length(); // month, quarterly, annually/ 7day, 15day, 30day

    jk_sticky_tab_bar(); // make the tab bar sticky.

    $(".plan_lengths li.active").trigger( "click" );
}) // doc.ready

$(window).on('resize',function() {
    jk_sticky_tab_bar(); // make the tab bar sticky.
});

function jk_sticky_tab_bar(){ // make the tab bar sticky.
    var sticky_outer =$('.plan_sec .tab_header');
    var sticky_item = $('.plan_sec .tab_header .sticky');
    var sticky_to_div = $('.plan_sec .tab_body');
    
    var div_location = sticky_outer.offset().top;
    var div_height = sticky_item.height();
    var sticky_to = sticky_to_div.offset().top + sticky_to_div.height() - div_height;
    
    
    $(window).scroll(function(){
        if ($(".mobile").is(":visible") == true) {
            var $this = $(this);
            var x = $this.scrollTop();
            if(x>=div_location && x< sticky_to){
                sticky_outer.css({'height': div_height});
                sticky_item.addClass('stuck');
                sticky_item.removeClass('fade');
            }else{
                if(x>=sticky_to){
                    sticky_item.addClass('fade');
                }else{
                    sticky_outer.css({'height': ''});
                    sticky_item.removeClass('stuck');
                }
            }
        }
    });
} // jk_sticky_tab_bar, make the tab bar sticky.

/* function jk_change_plan_type(){ // Subs or PAYG
    

    $('.plan_types li').click(function (event) {
        event.preventDefault();
        var $this = $(this);
        
            var ptype = $this.data('ptype');
            $this.addClass('active');
            $this.siblings('li').removeClass('active');
            $this.closest('.plan_sec').find('.ptype').addClass('hide');
            $this.closest('.plan_sec').find('.' + ptype).removeClass('hide');

            $this.closest('.plan_sec').find('.' + ptype + ' li.active').click();

        
    });

    
} // jk_change_plan_type, Subs or PAYG */


function jk_change_plan_length(){ // month, quarterly, annually/ 7day, 15day, 30day
    $('.plan_lengths .plan_length>li').click(function(event){
        var $this = $(this);
        

       
            var plan_length = $this.data('plength');
            var plan_type = $this.closest('.plan_length').data('ptype');

            $this.addClass('active');
            $this.siblings('li').removeClass('active');

            var lite_url = $this.data('lite'); 
            var premium_url = $this.data('premium'); 
            var premium_plus_url = $this.data('pplus'); 

            $this.closest('.plan_sec').find('.ptype.' + plan_type + '>.plength').addClass('hide');
            $this.closest('.plan_sec').find('.ptype.' + plan_type + '>.' + plan_length).removeClass('hide');

            $('.subs_lite').attr("href", lite_url); // change button url
            $('.subs_premium').attr("href", premium_url); // change button url
            $('.subs_premium_plus').attr("href", premium_plus_url); // change button url
        
    });
} // jk_change_plan_length   ... month, quarterly, annually/ 7day, 15day, 30day